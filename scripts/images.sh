#!/bin/bash

cat << EOF
1) To commit all running containers to the images for export them to tar files
2) To save all images to tar files
3) To load all tar files to images
4) To run all images
5) Remove directory for exported images
0) To exit
EOF

DIR="$HOME/docker-export-images"
mkdir -p $DIR

read option

case $option in
0 )
exit
;;

1 )
echo "you entered 1"
for i in $(docker ps --format "{{.Names}}") ; do
NAME=$i;
COMMAND=$(docker ps --filter="name=$i" --format "{{.Command}}");
IMAGE=$(docker ps --filter="name=$i" --format "{{.Image}}");
ID=$(docker ps --filter="name=$i" --format "{{.ID}}");
echo "$NAME $COMMAND" $IMAGE $ID; 
docker commit $ID $NAME;
docker save $NAME > $DIR/$NAME.tar;
done
;;

2 )
echo "you entered 2"
for i in $(docker images --format "{{.Repository}}"); do
IMAGE=$(echo $i | sed 's/\//_/')
docker save $i > $DIR/$IMAGE.tar;
done
;;

3 )
echo "you entered 3"
for i in $(ls $DIR); do
docker load <  $DIR/$i;
done
;;

4 )
echo "you entered 4"
for i in $(docker images --format "{{.Repository}}"); do
docker run -d $i;
done
;;

5 )
echo "you entered 5"
rm -rf $DIR
;;

esac
