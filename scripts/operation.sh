#!/bin/bash

cat <<EOF 
Choose your option:
1) Simple docker data backup
2) Docker temp backup for restoring data after storage driver changes
3) Cleanup backups
4) Docker data cleanup in case of Graph driver
5) Cleanup docker after devicemapper sdb and revert to graph
6) Cleanup docker after graph and revert to devicemapper
7) Restore from temp backup after storage driver reverted
0) Exit
EOF

read option

case $option in
0 )
exit
;;
1 )
echo "you enetered 1"
systemctl stop docker
cp -rp /var/lib/docker /var/lib/docker.backup-$(date +%d%m%y%H%M%S)
;;
2 )
echo "you enetered 2"
systemctl stop docker
rm -rf /var/lib/docker.backup
cp -pr /var/lib/docker /var/lib/docker.backup
systemctl start docker
;;
3 )
echo "you enetered 3"
rm -rf /var/lib/docker.backup*
;;
4 )
echo "you entered 4"
systemctl stop docker
rm -f /etc/sysconfig/docker-storage /etc/sysconfig/docker-storage-setup
rm -rf /var/lib/docker
systemctl start docker
;;
5 )
echo "you entered 5"
systemctl stop docker
rm -f /etc/sysconfig/docker-storage /etc/sysconfig/docker-storage-setup
rm -rf /var/lib/docker
dd if=/dev/zero of=/dev/sdb bs=512 count=1
echo "Please reboot"
;;
6 )
echo "you entered 6"
systemctl stop docker
rm -f /etc/sysconfig/docker-storage /etc/sysconfig/docker-storage-setup
rm -rf /var/lib/docker
dd if=/dev/zero of=/dev/sdb bs=512 count=1
cat <<EOF > /etc/sysconfig/docker-storage-setup
DEVS=/dev/sdb
VG=docker
EXTRA_DOCKER_STORAGE_OPTIONS='--storage-opt dm.blkdiscard=true'
EOF
echo "Please reboot"
;;
7 )
echo "you entered 7"
systemctl stop docker
rm -rf /var/lib/docker
cp -pr /var/lib/docker.backup /var/lib/docker
systemctl start docker
;;
esac
